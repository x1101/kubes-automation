#!/bin/bash
TYPE=$1
NAME=$2
STATE=$3
case $STATE in
	"MASTER") /usr/bin/systemctl start haproxy.service
						;;
	"BACKUP") /usr/bin/systemctl stop haproxy.service
						;;
	"FAULT")  /usr/bin/systemctl stop haproxy.service
						exit 0
						;;
	*)        /sbin/logger "ipsec unknown state"
						exit 1
						;;
esac
