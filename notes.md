# Init the Control Plane
kubeadm init --upload-certs --ignore-preflight-errors=all --config kubeadm-config.yaml

# Post-Init Steps
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config

# Setup the Flannel Networking model
kubectl apply -f kube-flannel.yml

# Reboot the node. This shouldn't be needed, but it seems to be
reboot

# Setup kubectl autocompletion
kubectl completion bash > /etc/bash_completion.d/kubectl; source /etc/bash_completion/kubectl
kubeadm completion bash > /etc/bash_completion.d/kubeadm; source /etc/bash_completion.d/kubeadm;

# Join Other Nodes to Control Plane
echo "$(kubeadm token create --print-join-command) --control-plane --certificate-key $(kubeadm init phase upload-certs --upload-certs | grep -vw -e certificate -e Namespace)"

# Reboot the node. This shouldn't be needed, but it seems to be
reboot

# Add the work role to all nodes
for node in `kubectl get nodes | awk '{print $1}' | tail +2`; do kubectl label nodes ${node} node-role.kubernetes.io/worker=worker; done

# Setup shared filesystem (super hax for now)
## This needs to be setup in Virtualbox per-VM before rolling it out
mkdir -p /pods
mount -t vboxsf pods /pods

# Setup The Volume Provisioner
kubectl apply -f local-volume-provisioner-manual.yaml

# Setup Persistent Volume "/pods"
kubectl apply -f pv-volume.yaml

# Taint the nodd (hax/temporary while running portainer in the control plane)
kubectl taint nodes test00 node-role.kubernetes.io/control-plane-
kubectl taint nodes test00 node-role.kubernetes.io/master-

# Portainer
kubectl apply -f portainer.yaml
